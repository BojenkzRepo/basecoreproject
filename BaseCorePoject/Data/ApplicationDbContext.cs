﻿using BaseCoreProject.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BaseCoreProject.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, CustomRole, int>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>().ToTable("UserIdentity").Property(p => p.Id).HasColumnName("UserId");
            //builder.Entity<CustomUserRole>().ToTable("UserRoleIdentity").HasKey(p => new { p.RoleId, p.UserId });
            //builder.Entity<CustomUserLogin>().ToTable("UserLoginIdentity").HasKey(p => new { p.LoginProvider, p.ProviderKey, p.UserId });
            //builder.Entity<CustomUserClaim>().ToTable("UserClaimIdentity").HasKey(p => p.Id);
            //builder.Entity<CustomRole>().ToTable("RoleIdentity").Property(p => p.Id).HasColumnName("RoleId");
        }
    }
}
