﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using BaseCoreProject.Data;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Builder;

namespace BaseCoreProject.Models
{
    public class CustomUserLogin : IdentityUserLogin<int> { };
    public class CustomUserRole : IdentityUserRole<int> { };
    public class CustomUserClaim : IdentityUserClaim<int> { };
    public class CustomUserToken : IdentityUserToken<int> { };
    public class CustomRoleClaim : IdentityRoleClaim<int> { };
    
    public class CustomRole : IdentityRole<int>
    {
        public CustomRole() { }
        public CustomRole(string name) { Name = name; }
    }

    public partial class ApplicationUser : IdentityUser<int>
    {
    }
}
