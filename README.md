# README #

this is base project for dot net core with customize application User / User Identity.

User Identity using int as primary key.
Only support SQL server, there still no connector for mysql.

### What is this repository for? ###
This repo is intended for developer who want's to develop application with Identity user primary key as integer, not string as default value. :)

### How do I get set up? ###
just clone the code and rebuild the application.
Please don't forget to change the connection string on project.json file.

### Contribution guidelines ###

just build with debug / release mode..